var user_email;
function htmlspecialchars(ch) {
  if (ch===null) return '';
  ch = ch.replace(/&/g,"&amp;");
  ch = ch.replace(/\"/g,"&quot;");
  ch = ch.replace(/\'/g,"&#039;");
  ch = ch.replace(/</g,"&lt;");
  ch = ch.replace(/>/g,"&gt;");
  return ch;
}
function init() {
    user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "<br></span><span><a class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnlogout = document.getElementById("logout-btn");
            btnlogout.addEventListener('click', function () {
                    firebase.auth().signOut()
                    .then(function(){
                        window.location.herf = "nthu.html";
                    }
                    )
                    .catch(function(error){})
                    //window.location.href = "index.html";   
                             
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='index.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            postsRef.push({email:user_email, data:htmlspecialchars(post_txt.value)});
            //document.getElementById('post_list').innerHTML +=  str_before_username + user_email + "</strong>" + post_txt.value + str_after_content;
            post_txt.value = "";
        }
        notifyMe('nthu', user_email);
    });

    // The html code for post
    var str_before_username = "<div class='w3-card-4 w3-margin w3-white'><div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>&nbsp;";
    var str_after_content = "</p></div></div>\n<hr>";

    var postsRef = firebase.database().ref('com_list_nthu');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            var newlist = document.getElementById('post_list');
            var num = 1;
            for(var i in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
               console.log(i);
               total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>&nbsp;&nbsp;&nbsp;&nbsp;" + snapshot.val()[i].data + str_after_content + "<div id = 'reply_list"+num+"'></div>&nbsp;&nbsp;<textarea class='form-control' onclick = 'commentFunction("+num+")' rows='1' cols='45' id='reply"+num+"' style='resize:none'></textarea>&nbsp;<div>&nbsp;&nbsp;<button id='btn" + num + "' type='button' class='btn btn-primary'>Submit</button></div></div>");
                //console.log(total_post.push());
                num++;
            }
            newlist.innerHTML = total_post.join('');


            postsRef.on('value', function(snapshot){
                total_post = [];
                newlist.innerHTML = "";
                var num = 1;
              //  var newlist = document.getElementById('post_list');
            for(var i in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list");
               total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>&nbsp;&nbsp;&nbsp;&nbsp;" + snapshot.val()[i].data + str_after_content + "<div id = 'reply_list"+num+"'></div>&nbsp;&nbsp;<textarea class='form-control' onclick = 'commentFunction("+num+")' rows='1' cols='45' id='reply"+num+"' style='resize:none'></textarea>&nbsp;<div>&nbsp;&nbsp;<button id='btn" + num + "' type='button' class='btn btn-primary'>Submit</button></div></div>");
            //    total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>" + snapshot.val()[i].data + str_after_content + "<button id='reply_btn' type='button' class='btn btn-success' onclick = 'commentFunction(" + num + ")'>Comment</button><div id = 'reply_list"+num+"'></div><textarea class='form-control' rows='1' id='reply"+num+"' style='resize:none'></textarea><button id='btn" + num + "' type='button' class='btn btn-success'>Submit</button></div>");
              //  console.log(snapshot.val()[i]);
              //  console.log(i);
              num++;
            }
            newlist.innerHTML = total_post.join('');
            })
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();

};

function commentFunction(i){
    console.log(i);
    var tmp;
    var postsRef = firebase.database().ref('com_list_nthu');
    postsRef.once('value')
        .then(function (snapshot) {
            var num = 1;
            for(var j in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
              // console.log(i);
              if(num == i) {
                  //console.log(j);
                 // console.log(snapshot.val()[j]);
                 // replyRef = firebase.database().ref('com_list/'+j);
                tmp = j;
                console.log(tmp);
                break;
              }
                //total_post.push(str_before_username + snapshot.val()[i].email + "</strong>" + snapshot.val()[i].data + str_after_content + "<button id='reply_btn' type='button' class='btn btn-success' onclick = 'commentFunction(" + num + ")'>Comment</button></div>");
                //console.log(total_post.push());
                num++;
            }
           // newlist.innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));


    btn = document.getElementById('btn'+i);
    reply = document.getElementById('reply'+i);
    btn.addEventListener('click', function () {
        if (reply.value != "") {
            replyRef.push({email:user_email, data:htmlspecialchars(reply.value)});
            //document.getElementById('post_list').innerHTML +=  str_before_username + user_email + "</strong>" + post_txt.value + str_after_content;
            reply.value = "";
        }
    });

    // The html code for post
    var str_before_username = "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p>\n";
    var replyRef = firebase.database().ref('list'+i+'_nthu');
    // List for store posts html
    var total_reply = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    replyRef.once('value')
        .then(function (snapshot) {
            var newlist = document.getElementById('reply_list'+i);
            var num = 1;
            for(var j in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
               //console.log(i);
                total_reply.push(str_before_username + snapshot.val()[j].email + "</strong>&nbsp;&nbsp;" + snapshot.val()[j].data + str_after_content);
                //console.log(total_post.push());
                num++;
            }
            newlist.innerHTML = total_reply.join('');


            replyRef.on('value', function(snapshot){
                total_reply = [];
                newlist.innerHTML = "";
                var num = 1;
              //  var newlist = document.getElementById('post_list');
              for(var j in snapshot.val()){
                // var listRef = firebase.database().ref("com_list/"+i+"/list)");
                //console.log(i);
                 total_reply.push(str_before_username + snapshot.val()[j].email + "</strong>&nbsp;&nbsp;" + snapshot.val()[j].data + str_after_content);
                 //console.log(total_post.push());
                 num++;
             }
             newlist.innerHTML = total_reply.join('');
            })
        })
        .catch(e => console.log(e.message));



    
}

function goToFunction(){
    window.location.herf = "first.html";
}


// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
  
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });
  
  function notifyMe(titleName, email) {
    console.log("test");
    if (Notification.permission !== "granted"){
  
    
      Notification.requestPermission();
      console.log('no');
    }  else {
      var notification = new Notification(titleName, {
        icon: 'img/nthu.png',
        body: email+"\npost a new article",
      });
  
      notification.onclick = function () {
        window.open("nthu.html");      
      };
  
    }
  
  }