function classHas(base, has) {
    const arr = base.split(" ")
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === has) 
        return true;
    }
  }
  
  function classReplace(base, replace, next) {
    return base.replace(replace, next)
  }
  
  function onBtnClick() {
    const block = document.getElementById("block");
    const btn = document.getElementById("btn");
    const overlay = document.getElementById("overlay");
  
    if (classHas(block.className, "div-show")) {
      block.className = classReplace(block.className, "div-show", "div-hidden")
      btn.innerHTML = "Show"
      const content = block.innerHTML;
      overlay.innerHTML = "<div class=\"div-overlay div-overlay-left\">" + content + "</div>" + "<div class=\"div-overlay div-overlay-right\">" + content + "</div>";
    } else {
      block.className = classReplace(block.className, "div-hidden", "div-show")
      btn.innerHTML = "Thanos Snap"
      overlay.innerHTML = null;
    }
  }
  