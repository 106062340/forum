function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var provider = new firebase.auth.GoogleAuthProvider();
    

    btnLogin.addEventListener('click', function () {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function (result) {
            window.location.href = "first.html";            
        }).catch(function (error) {
            // Handle Errors here.
            create_alert("error", "!!!!!!!!!!!");
        });
        
    });


    btnGoogle.addEventListener('click', function () {
        console.log("test");
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.

            // var token = result.credential.accessToken;
            // The signed-in user info.
            // var user = result.user;
                window.location.assign ("first.html");
            }).catch(function(error) {
            // Handle Errors here.
            // var errorCode= error.code;
            // var errorMessage= error.message;
            // The email of the user's account used.
            // var email = error.email;
            // The firebase.auth.AuthCredentialtype that was used.
            // var credential = error.credential;
            create_alert("error", "email fail");
            });
    });

    btnSignUp.addEventListener('click', function () {        
    firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).catch(function (error) {
        // Handle Errors here.
        varerrorCode = error.code;
        varerrorMessage = error.message;
    });
    txtEmail.value = "";
    txtPassword.value = "";
});
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};