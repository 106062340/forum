var user_email;
function init() {
    user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            //console.log("in");
            //console.log(user_email);
            var newlist = document.getElementById('email');
            newlist.innerHTML = "<p class='h3 mb-3 font-weight-normal'><i class='fa fa-envelope fa-fw w3-margin-right w3-text-theme'></i>Email:&nbsp;"+ user_email +"</p>";
        } else {
            user_email = 'user';
           // console.log("in_2");
        }
    });

    // var str_before = "<div class='my-3 p-3 bg-white rounded box-shadow' style='width:400px;height:auto; overflow-y:auto;'><p class='media-body pb-3 mb-0 small lh-125 border-gray' style='float:left'>";
    var str_after;
    var postsRef_gossip = firebase.database().ref('com_list_gossip');
    var postsRef_joke = firebase.database().ref('com_list_joke');
    var postsRef_nthu = firebase.database().ref('com_list_nthu');
    var postsRef_shopping = firebase.database().ref('com_list_shoping');
    var postsRef_career = firebase.database().ref('com_list_career');
    var postsRef_3C = firebase.database().ref('com_list_3C');
    var total_post = [];

    postsRef_gossip.once('value')
        .then(function (snapshot) {
            var newlist = document.getElementById('posts');
            // var num = 1;
            for(var i in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
            //    console.log(i);
               console.log(user_email);
               if(snapshot.val()[i].email == user_email){
                    total_post.push("<div class='w3-container w3-card w3-white w3-round w3-margin'><br><h4>Gossip</h4><hr class='w3-clear'><p>" + snapshot.val()[i].data +"</p></div>")
               }
            //    total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>&nbsp;&nbsp;&nbsp;&nbsp;" + snapshot.val()[i].data + str_after_content + "<div id = 'reply_list"+num+"'></div>&nbsp;&nbsp;<textarea class='form-control' onclick = 'commentFunction("+num+")' rows='1' cols='45' id='reply"+num+"' style='resize:none'></textarea>&nbsp;<div>&nbsp;&nbsp;<button id='btn" + num + "' type='button' class='btn btn-primary'>Submit</button></div></div>");
                //console.log(total_post.push());
                // num++;
            }
            newlist.innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));
    
    postsRef_joke.once('value')
        .then(function (snapshot) {
            var newlist = document.getElementById('posts');
            // var num = 1;
            for(var i in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
            //    console.log(i);
               console.log(user_email);
               if(snapshot.val()[i].email == user_email){
                    total_post.push("<div class='w3-container w3-card w3-white w3-round w3-margin'><br><h4>Joke</h4><hr class='w3-clear'><p>" + snapshot.val()[i].data +"</p></div>")
               }
            //    total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>&nbsp;&nbsp;&nbsp;&nbsp;" + snapshot.val()[i].data + str_after_content + "<div id = 'reply_list"+num+"'></div>&nbsp;&nbsp;<textarea class='form-control' onclick = 'commentFunction("+num+")' rows='1' cols='45' id='reply"+num+"' style='resize:none'></textarea>&nbsp;<div>&nbsp;&nbsp;<button id='btn" + num + "' type='button' class='btn btn-primary'>Submit</button></div></div>");
                //console.log(total_post.push());
                // num++;
            }
            newlist.innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));

    postsRef_nthu.once('value')
        .then(function (snapshot) {
            var newlist = document.getElementById('posts');
            // var num = 1;
            for(var i in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
            //    console.log(i);
               console.log(user_email);
               if(snapshot.val()[i].email == user_email){
                    total_post.push("<div class='w3-container w3-card w3-white w3-round w3-margin'><br><h4>NTHU</h4><hr class='w3-clear'><p>" + snapshot.val()[i].data +"</p></div>")
               }
            //    total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>&nbsp;&nbsp;&nbsp;&nbsp;" + snapshot.val()[i].data + str_after_content + "<div id = 'reply_list"+num+"'></div>&nbsp;&nbsp;<textarea class='form-control' onclick = 'commentFunction("+num+")' rows='1' cols='45' id='reply"+num+"' style='resize:none'></textarea>&nbsp;<div>&nbsp;&nbsp;<button id='btn" + num + "' type='button' class='btn btn-primary'>Submit</button></div></div>");
                //console.log(total_post.push());
                // num++;
            }
            newlist.innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));

    postsRef_shopping.once('value')
        .then(function (snapshot) {
            var newlist = document.getElementById('posts');
            // var num = 1;
            for(var i in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
            //    console.log(i);
               console.log(user_email);
               if(snapshot.val()[i].email == user_email){
                    total_post.push("<div class='w3-container w3-card w3-white w3-round w3-margin'><br><h4>Shopping</h4><hr class='w3-clear'><p>" + snapshot.val()[i].data +"</p></div>")
               }
            //    total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>&nbsp;&nbsp;&nbsp;&nbsp;" + snapshot.val()[i].data + str_after_content + "<div id = 'reply_list"+num+"'></div>&nbsp;&nbsp;<textarea class='form-control' onclick = 'commentFunction("+num+")' rows='1' cols='45' id='reply"+num+"' style='resize:none'></textarea>&nbsp;<div>&nbsp;&nbsp;<button id='btn" + num + "' type='button' class='btn btn-primary'>Submit</button></div></div>");
                //console.log(total_post.push());
                // num++;
            }
            newlist.innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));

    postsRef_career.once('value')
        .then(function (snapshot) {
            var newlist = document.getElementById('posts');
            // var num = 1;
            for(var i in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
            //    console.log(i);
               console.log(user_email);
               if(snapshot.val()[i].email == user_email){
                    total_post.push("<div class='w3-container w3-card w3-white w3-round w3-margin'><br><h4>Career</h4><hr class='w3-clear'><p>" + snapshot.val()[i].data +"</p></div>")
               }
            //    total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>&nbsp;&nbsp;&nbsp;&nbsp;" + snapshot.val()[i].data + str_after_content + "<div id = 'reply_list"+num+"'></div>&nbsp;&nbsp;<textarea class='form-control' onclick = 'commentFunction("+num+")' rows='1' cols='45' id='reply"+num+"' style='resize:none'></textarea>&nbsp;<div>&nbsp;&nbsp;<button id='btn" + num + "' type='button' class='btn btn-primary'>Submit</button></div></div>");
                //console.log(total_post.push());
                // num++;
            }
            newlist.innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));

    postsRef_3C.once('value')
        .then(function (snapshot) {
            var newlist = document.getElementById('posts');
            // var num = 1;
            for(var i in snapshot.val()){
               // var listRef = firebase.database().ref("com_list/"+i+"/list)");
            //    console.log(i);
               console.log(user_email);
               if(snapshot.val()[i].email == user_email){
                    total_post.push("<div class='w3-container w3-card w3-white w3-round w3-margin'><br><h4>3C</h4><hr class='w3-clear'><p>" + snapshot.val()[i].data +"</p></div>")
               }
            //    total_post.push(str_before_username + snapshot.val()[i].email + "</strong></p></div><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>&nbsp;&nbsp;&nbsp;&nbsp;" + snapshot.val()[i].data + str_after_content + "<div id = 'reply_list"+num+"'></div>&nbsp;&nbsp;<textarea class='form-control' onclick = 'commentFunction("+num+")' rows='1' cols='45' id='reply"+num+"' style='resize:none'></textarea>&nbsp;<div>&nbsp;&nbsp;<button id='btn" + num + "' type='button' class='btn btn-primary'>Submit</button></div></div>");
                //console.log(total_post.push());
                // num++;
            }
            newlist.innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));




}
window.onload = function () {
    init();

};
