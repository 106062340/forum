# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 106062340 Forum
* Key functions (add/delete)
    1. sign in page
    2. Post page
    3. Post list page
    4. leave comment under any post
    5. user page
    6. RWD
    
* Other functions (add/delete)
    1. animation (Thanos effect)
    2. user page (have all the posts of the user in the page) 
    3. prevent from key in HTML code (security)
    4. google notifacation

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# webside  https://midterm-39b00.firebaseapp.com/

# Components Description : 
0. Basic : Sign in(can use "su@gmail.com" ,"willie" ), sign out, third-party web service,firebase deploy ,database read/write,RWD

1. sign in page : There are three bottum in the page. First is Sign in, and Google sign in, and new account. The Function is like what we have done in lab06.

2. Post list page : after sign in is the post list page, which shows every board of the forum. And there is a button called "Thanos snap", can bring the animation(click it and the below would disappear, again, show). On the up-right there are two bottum. The left one links to the user page while the right one is log out.

3. user page : there are three columns of the page. The left show the email of current user and all the board. The middle shows all posts the user post on every board(post page). The right column shows the project deadline. XDD (click the up-left "home" bottum to get back)

4. post page : all six boards have the same function. can post new post (and make google notifacation),and click the bar below the post, can see all the comments and make one as well. the right column has two block. the upper show the email and can click "log out" to log out. the other lists all the board and home page(post list page), can click and link to the page.


# Other Functions Description(1~10%) : 
1. the user page can see all the post of the user
2. the animation

## Security Report (Optional)
can prevent from key in html code.

function htmlspecialchars(ch) {
  if (ch===null) return '';
  ch = ch.replace(/&/g,"&amp;");
  ch = ch.replace(/\"/g,"&quot;");
  ch = ch.replace(/\'/g,"&#039;");
  ch = ch.replace(/</g,"&lt;");
  ch = ch.replace(/>/g,"&gt;");
  return ch;
}
